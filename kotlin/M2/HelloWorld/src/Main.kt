fun main(args: Array<String>) {
    val firstName = "Nikolay"
    val secondName = "Bozhok"
    var age = 25
    var weight: Float = 70.0f
    var height = 185
    var isAdult: Boolean = true

    if (height <= 150 || weight <= 45) {
        isAdult = false
    }

    var info = """
        User name: $firstName $secondName
        ${age}y.o., ${weight}kg, ${height}cm
        Identified as: ${if (isAdult) "Adult" else "Children"}
    """

    print(info)

    height++
    weight++

    info = """
        User name: $firstName $secondName
        ${age}y.o., ${weight}kg, ${height}cm
        Identified as: ${if (isAdult) "Adult" else "Children"}
    """

    print(info)
}