import kotlin.math.sqrt

fun main(args: Array<String>) {
    print(solveEquation(3, -14, -5))                //3 * x^2 - 14x * x - 5 = 0
}

fun solveEquation(a: Int, b: Int, c: Int): Any {
    val D: Int = (b * b) - 4 * a * c

    if (D > 0) {
        val x1 = (-b + sqrt(D.toDouble())) / (2 * a)         //Расчёт корней
        val x2 = (-b - sqrt(D.toDouble())) / (2 * a)
        println("x1 $x1")
        println("x2 $x2")
        return x1 + x2
    }
    return if (D == 0) {
    } else {
        "None"
    }
}
