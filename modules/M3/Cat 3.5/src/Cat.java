
public class Cat
{
    private double originWeight;
    public double weight;

    private static final int CAT_EYES_AMOUNT = 2;
    public static final double MIN_WEIGHT = 1000;
    public static final  double MAX_WEIGHT = 9000;

    //public String name;

    private boolean isCatAlive;

    public Cat()
    {
        //weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;

    }

    public Cat (Double amount) {
        this();
        this.weight = amount;
    }

    public void meow()
    {
        if (isCatAlive = true) {
            weight = weight - 1;
            System.out.println("Meow");
        }
        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public void feed(Double amount)
    {
        if (isCatAlive = true) {

            weight = weight + amount;
        }

        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public void drink(Double amount)
    {
        if (isCatAlive = true) {

            weight = weight + amount;
        }
        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public Double getWeight()
    {
        return weight;
    }

    public String getStatus()
    {
        if(weight < MIN_WEIGHT) {
            return "OVERMEOW";
        }
        else if(weight > MAX_WEIGHT) {
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Chillin'";
        }
        else {
            return "Waiting'";
        }
    }
}