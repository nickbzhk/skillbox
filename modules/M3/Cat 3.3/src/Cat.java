
public class Cat
{
    private double originWeight;
    public double weight;

    private static final int CAT_EYES_AMOUNT = 2;
    public static final double MIN_WEIGHT = 1000;
    public static final  double MAX_WEIGHT = 9000;

    public static int count;
    private boolean isCatAlive;

    public Cat()
    {
        weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;
        count = count + 1;
    }

    public boolean isCatAlive () {
        if ((weight < MIN_WEIGHT) || (weight > MAX_WEIGHT)) {
            return false;
        }
        else return true;
    }

    public static int getCount() {

        return count;
    }

    public void meow()
    {
        if (isCatAlive = true) {
            weight = weight - 1;
            System.out.println("Meow");
        }
        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public void feed(Double amount)
    {
        if (isCatAlive = true) {

            weight = weight + amount;
        }

        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public void drink(Double amount)
    {
        if (isCatAlive = true) {

            weight = weight + amount;
        }
        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public Double getWeight()
    {
        return weight;
    }

    public String getStatus()
    {
        if(weight < MIN_WEIGHT) {
            count -= count - 1;
            isCatAlive = false;
            return "OVERMEOW";
        }
        else if(weight > MAX_WEIGHT) {
            count -= count - 1;
            isCatAlive = false;
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Chillin'";
        }
        else {
            return "Waiting'";
        }
    }
}