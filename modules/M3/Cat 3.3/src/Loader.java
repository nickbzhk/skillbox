
public class Loader
{
    public static void main(String[] args)
    {

        Cat cat1 = new Cat();
        Cat cat2 = new Cat();

        System.out.println(cat1.getStatus() + cat1.getWeight());
        System.out.println(Cat.getCount());

        do {
            cat1.feed(1.0);
        } while (cat1.weight <= 9000);

        System.out.println(cat1.getStatus() + cat1.getWeight());
        System.out.println(Cat.getCount());

        cat1.meow();

        System.out.println(cat1.getStatus() + cat1.getWeight());
        System.out.println(Cat.getCount());

    }
}