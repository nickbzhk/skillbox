
public class Loader
{
    public static void main(String[] args)
    {

        Cat cat1 = new Cat();
        Cat cat2 = new Cat();
        Cat cat3 = new Cat();
        Cat cat4 = new Cat();
        Cat cat5 = new Cat();

        double i = 9000.0;

        System.out.println("Cat 1: " + cat1.getStatus() + " - Weight: " + cat1.getWeight());
        System.out.println("Cat 2: " + cat2.getStatus() + " - Weight: " + cat2.getWeight());
        System.out.println("Cat 3: " + cat3.getStatus() + " - Weight: " + cat3.getWeight());
        System.out.println("Cat 4: " + cat4.getStatus() + " - Weight: " + cat4.getWeight());
        System.out.println("Cat 5: " + cat5.getStatus() + " - Weight: " + cat5.getWeight());

        cat1.feed(i); //кормёжка
        cat3.feed(1.0);
        cat5.feed(1.0);

        //суицид
        do {
            cat4.meow();
        } while (cat4.getWeight() >= 1000);

        System.out.println("Cat 1: " + cat1.getStatus() + " - Weight: " + cat1.getWeight());
        System.out.println("Cat 2: " + cat2.getStatus() + " - Weight: " + cat2.getWeight());
        System.out.println("Cat 3: " + cat3.getStatus() + " - Weight: " + cat3.getWeight());
        System.out.println("Cat 4: " + cat4.getStatus() + " - Weight: " + cat4.getWeight());
        System.out.println("Cat 5: " + cat5.getStatus() + " - Weight: " + cat5.getWeight());

    }
}