
public class Cat
{
    private double originWeight;
    public double weight;

    public double minWeight;
    private double maxWeight;
    public double foodWeight;

    public Cat()
    {
        weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;
        minWeight = 1000.0;
        maxWeight = 9000.0;

    }

    public void meow()
    {
        weight = weight - 1;
        System.out.println("Meow");
    }

    public void pee()
    {
        weight = weight - 1;
        System.out.println("Peeing... Done. Call the cops");
    }

    public void feed(Double amount)
    {

        weight = weight + amount;
        foodWeight += amount;
    }

    public void drink(Double amount)
    {

        weight = weight + amount;
    }

    public Double getWeight()
    {

        return weight;
    }

    public String getStatus()
    {
        if(weight < minWeight) {
            return "OVERMEOW";//ex Dead
        }
        else if(weight > maxWeight) {
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Chillin'"; //ex Sleeping
        }
        else {
            return "Waiting'";//ex Playing
        }
    }
    public Double eatenAmount() {

        return foodWeight;
    }
}