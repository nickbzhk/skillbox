public class Loader
{
    public static void main(String[] args)
    {
        Cat cat1 = new Cat();

        System.out.println("Before: " + cat1.getWeight());

        cat1.feed(150.0);

        System.out.println("After: " + cat1.getWeight());
        System.out.println("Eaten: " + cat1.eatenAmount());

        cat1.pee();
        cat1.pee();
        cat1.pee();

        System.out.println(cat1.getWeight());
    }
}