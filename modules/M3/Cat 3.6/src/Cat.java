
public class Cat
{
    private double originWeight;
    public double weight;

    private static final int CAT_EYES_AMOUNT = 2;
    public static final double MIN_WEIGHT = 1000;
    public static final double MAX_WEIGHT = 9000;

    private boolean isCatAlive;
    public Colour colour;
    private static int count;


    public Cat()
    {
        setColour(Colour.RED);
        weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;
        count++;
    }

    public Cat(Double amount)
    {
        setColour(Colour.RED);
        weight = amount;
        originWeight = weight;
        count++;
    }

    public Cat copy(){
        return new Cat(getWeight());
    }

    public void setColour (Colour colour)
    {

        this.colour = Colour.RED;
    }

    public Colour getColour()
    {
        return colour;
    }

    private boolean isCatAlive() {
        if ((weight < MIN_WEIGHT) || (weight > MAX_WEIGHT)){
            count--;
            return false;
        }
        else return true;
    }

    public static int getCount(){
        return count;
    }

    public void meow()
    {
        if (isCatAlive()) {
            weight = weight - 1;
            System.out.println("Meow");
        }
        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public void feed(Double amount)
    {
        if (isCatAlive()) {

            weight = weight + amount;
        }

        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public void drink(Double amount)
    {
        if (isCatAlive()) {

            weight = weight + amount;
        }
        else  if (!isCatAlive) {
            System.out.println("Cat is dead...");
        }
    }

    public Double getWeight()
    {
        return weight;
    }

    public String getStatus()
    {
        if(weight < MIN_WEIGHT) {
            return "OVERMEOW";
        }
        else if(weight > MAX_WEIGHT) {
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Chillin'";
        }
        else {
            return "Waiting'";
        }
    }
}