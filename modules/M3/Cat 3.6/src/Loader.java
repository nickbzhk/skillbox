import java.time.LocalDate;

public class Loader
{
    public static void main(String[] args) {


        Cat cat1 = new Cat(); //рандомная генерация
        Cat cat2 = new Cat(1200.00); //напрямую через конструктор
        Cat cat3 = getKitten(1100.00); //через метод генерации с использованием конструктора


        //"глубокая" копия
        Cat originalCat = new Cat();
        Cat copyCat = originalCat.copy();


        System.out.println( cat1.getWeight());
        System.out.println( cat2.getWeight());
        System.out.println( cat3.getWeight());
        System.out.println("Original cat: " + originalCat.getWeight());
        System.out.println("Copy: " + copyCat.getWeight());

        System.out.println(Cat.getCount());

        ////////////////////////////////////////////////////////////////////////

        do {
            cat1.feed(100.);
        } while (cat1.weight <=9000.00);

        cat1.feed(100.); //контрольный дожор

        System.out.println( cat1.getWeight());
        System.out.println(Cat.getCount()); //контрольный подсчёт

    }

    public static Cat getKitten(Double amount){
        return new Cat(amount);
    }
}
