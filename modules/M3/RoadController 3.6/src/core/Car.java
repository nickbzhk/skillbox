package core;

public class Car
{
    public String number;
    public int height;
    public double weight;
    public boolean hasVehicle;
    public boolean isSpecial;

    public void setNumber(String number) {
        this.number = number;
    }
    public String getNumber() {
        return number;
    }
    public void setHeight (int amount) {
        this.height = amount;
    }
    public int getHeight () {
        return height;
    }

    public void setWeight (Double amount) {
        this.weight = amount;
    }
    public double getWeight () {
        return weight;
    }


    public void setVehicle(boolean value) {
        this.hasVehicle = value;
    }
    public boolean isHasVehicle() {
        return hasVehicle;
    }
    public void setSpecial(boolean value) {
        this.isSpecial = value;
    }
    public boolean getSpecial() {
        return isSpecial;
    }

    public String toString()
    {
        String special = isSpecial ? "СПЕЦТРАНСПОРТ " : "";
        return "\n=========================================\n" +
            special + "Автомобиль с номером " + number +
            ":\n\tВысота: " + height + " мм\n\tМасса: " + weight + " кг";
    }
}