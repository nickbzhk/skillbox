import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArrayListCMD {
    private static ArrayList<String> list = new ArrayList<>();

    public static void main(String[] args) {
        System.out.print(
                "Use commands (count starts at 0!): \n" +
                "\"LIST\" \n" +
                "\"ADD\" + index number (optional) \n" +
                "\"EDIT\" + index number \n" +
                "\"DEL\" + index number \n"
        );
        Scanner scanner = new Scanner(System.in);
        Pattern regex = Pattern.compile("(LIST|ADD|EDIT|DEL)\\s*(\\d*)\\s*(.*)");

        do {
            String input = scanner.nextLine();
            Matcher matcher = regex.matcher(input);

            if (matcher.find()) {
                String getCommand = matcher.group(1);
                int getIndex = matcher.group(2).isEmpty() ? -1 : Integer.parseInt(matcher.group(2));
                String getText = matcher.group(3);
                if (getCommand.equals("LIST")) {
                    getList();
                }
                if (getCommand.equals("ADD")) {
                    add(getIndex, getText);
                }
                if (getCommand.equals("EDIT")) {
                    edit(getIndex, getText);
                }
                if (getCommand.equals("DEL")) {
                    remove(getIndex);
                }
            }
        } while (true);
    }

    public static void add(int index, String text) {
        if (index == -1) {
            list.add(text);
            System.out.println("Item \"" + text + "\" has been added.");
        }
        if (index != -1) {
            list.add(index, text);
            System.out.println("Item \"" + text + "\" has been added at index #" + index);
        }

    }

    public static void edit(int index, String text) {
        if (index == -1) {
            System.out.println("Missing index!");
        }
        if (index != -1 && index < list.size()) {
            list.set(index, text);
            System.out.println("The item has been changed to \"" + text + "\" at index #" + index);
        } else {
            System.out.println("No such index! Only " + list.size() + " item(s) are available.");
        }
    }

    public static void remove(int index) {
        if (index == -1) {
            System.out.println("Missing index!");
        }
        if (index != -1 && index < list.size()) {
            list.remove(index);
            System.out.println("Item #" + index + " has been removed. \n" +
                    "There's " + list.size() + " item(s) in the list now.");
        } else {
            System.out.println("No such index! Only " + list.size() + " item(s) are available.");
        }
    }

    public static void getList() {
        if (list.size() != 0) {
            for (int i = 0; i <= list.size() - 1; i++) {
                int itemCounter;
                System.out.println((itemCounter = i) + ") " + list.get(i));
            }
        } else {
            System.out.println("The list is empty! Use \"ADD\" command to add items.");
        }
    }
}