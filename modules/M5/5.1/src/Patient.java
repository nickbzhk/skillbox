public class Patient {
    private double temperature;

    public Patient() {
        this.temperature = 35 + (5 * Math.random());
    }
    public Double getTemperature() {
        return temperature;
    }
}
