public class SickUnits {
    public static void main(String[] args) {

        Patient[] p = new Patient[30];

        for (byte i = 0; i <= 29; i++) {
            p[i] = new Patient();                                // Создание массива на i пациентов
        }

                                                                 // Out
        System.out.println(averageTemperature(p) + " - средняя температура по больнице");
        System.out.println(getHealthyPatientsCount(p) + " - колличество здоровых пациентов");
    }

    public static double averageTemperature(Patient[] p) {       // Суммирование температур
        double averageTemp = 0;

        for (byte j = 0; j <= 29; j++) {
            averageTemp = (averageTemp + p[j].getTemperature());
        }
        averageTemp = averageTemp / p.length;
        return averageTemp;
    }

    public static int getHealthyPatientsCount(Patient[] p) {
        byte healthyPatients = 0;
        for (byte y = 0; y <= 29; y++) {
            if (p[y].getTemperature() <= 36.9) {                // Кол-во здоровых пациентов
                healthyPatients = (byte) (healthyPatients + 1);
            }
        }
        return healthyPatients;
    }

}

