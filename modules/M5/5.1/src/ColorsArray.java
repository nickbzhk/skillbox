import java.util.Arrays;

public class ColorsArray {
    public static void main(String[] args) {
        String quote = "Широкий спектр слов, который можно было вписать в эту строку";
        String[] words = quote.split("\\s+");

        for (int i = 0; i < words.length / 2; i++) {
            String buffer = words[i];
            words[i] = words[words.length - i - 1];
            words[words.length - i - 1] = buffer;
        }
        for (String word : words) {
            quote = word + " ";
            System.out.print(quote);
        }
    }
}
