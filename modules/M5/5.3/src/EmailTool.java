import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailTool {
    private static HashSet<String> list = new HashSet<>();
    public static void main(String[] args) {
        System.out.println(
                    "\n" +
                    "Type commands: \n" +
                    "\u2022 \"ADD\" to add new items, \n" +
                    "\u2022 \"LIST\" to see added items.");

        Scanner scanner = new Scanner(System.in);
        final Pattern REGEX = Pattern.compile("(LIST|ADD)\\s*(.*)");

        for (; ; ) {
            System.out.print(">:");
            final String INPUT = scanner.nextLine();
            final Matcher MATCHER = REGEX.matcher(INPUT);

            if (MATCHER.find()) {
                String getCommand = MATCHER.group(1);
                String getText = MATCHER.group(2);
                if (getCommand.equals("LIST")) {
                    getList();
                }
                if (getCommand.equals("ADD")) {
                    add(getText);
                }
            }
        }
    }

    final private static String ERROR_MESSAGE =
                    "\n" +
                    "Email is not valid! Please, check spelling for these errors:\n" +
                    "\u2022 Double symbols (dots, @'s, etc.),\n" +
                    "\u2022 Dots at Email's name start/end,\n" +
                    "\u2022 Missing \"@\",\n" +
                    "\u2022 Non latin character was used.";

    public static void add(String text) {
        if (list.contains(text)) {
            System.out.println("\n" + "This item already exists!");
        } else if (isValid(text)) {
            list.add(text);
            System.out.println("\n" + "Item \"" + text + "\" has been added.");
        } else {
            System.out.println(ERROR_MESSAGE);
        }
    }

    public static void getList() {
        if (list.size() != 0) {
            System.out.println("\n" + list);
        } else {
            System.out.println("\n" + "The list is empty! Use \"ADD\" command to add items.");
        }
    }

    public static boolean isValid (String email) {
        final String REGEX_RU = "([А-Яа-я])";
        final Pattern PATTERN = Pattern.compile(REGEX_RU);
        final Matcher MATCHER = PATTERN.matcher(email);
        int atIndex = email.indexOf("@");

        if (
                !MATCHER.find() &&
                email.contains("@") &&
                email.substring(0, atIndex).length() < 64 &&
                email.substring(atIndex, email.lastIndexOf('.')).length() < 256 &&
                !email.substring(atIndex, atIndex + 1).startsWith(".") &&
                !email.substring(0, atIndex).endsWith(".") &&
                !email.startsWith(".") &&
                !email.contains("@@") &&
                !email.contains(" ") &&
                !email.contains("..")) {
            return true;
        } else {
            return false;
        }
    }
}