import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactsMap {
    final private static TreeMap<String, String> CONTACTS_MAP = new TreeMap<>();

    public static void main(String[] args) {

        for (; ; ) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("\n >:");
            String command = scanner.nextLine();

            if (command.contains("LIST")) {
                list();
            } else {

                String input = command.replace("ADD", "");
                Pattern numberRegex = Pattern.compile("\\d+");
                Matcher numberMatch = numberRegex.matcher(input);

                String name = "";
                String number = "";

                name = input.replaceAll("\\d", "");
                while (numberMatch.find()) {
                    number = numberMatch.group(0);
                }

                //Вывод всей информации о контакте
                if (CONTACTS_MAP.containsKey(name)) {
                    System.out.println(name + " " + CONTACTS_MAP.get(name));
                    continue;
                } else if (CONTACTS_MAP.containsValue(number)) {
                    details(number);
                    continue;
                }

                //Введены имя и номер
                if (!name.isEmpty() && !number.isEmpty()) {
                    add(name, number);
                }
                //Введён только номер
                if (name.isEmpty() && !number.isEmpty()) {
                    System.out.print("Number " + number + " was found, put contact name: \n" + ">:");
                    name = scanner.nextLine();
                    add(name, number);
                }
                //Введено только имя
                if (!name.isEmpty() && number.isEmpty()) {
                    System.out.print("Name " + name + " was found, put contact number: \n" + ">:");
                    number = scanner.nextLine();
                    add(name, number);
                }
            }
        }
    }

    private static void add(String name, String number) {
        if (PhoneNumber.isMobilePhoneNumber(number)) {
            number = PhoneNumber.correctNumber(number);
        }
        CONTACTS_MAP.put(name, number);
        System.out.println("Item \"" + name + " " + number + "\" has been added.");
    }

    private static void list() {
        if (!(CONTACTS_MAP.size() == 0)) {
            for (Map.Entry<String, String> entry : CONTACTS_MAP.entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());
            }
        } else if ((CONTACTS_MAP.size() == 0)) {
            System.out.println("The list is empty!");
        }
    }

    private static void details(String number) {
        for (Map.Entry<String, String> entry : CONTACTS_MAP.entrySet()) {
            if (entry.getValue().equals(number)) {
                System.out.println(entry.getKey() + " " + number);
            }
        }
    }
}