import java.util.*;

public class DataTest {
    public static void main(String[] args) {
        String nomerok = "";
        String alphabet = "ABCEHKMOPTX";

        ArrayList<String> list = new ArrayList<>();

        for (int a = 1; a <= 197; a++) {                  // region
            for (int b = 0; b <= 9; b++) {                // numbers code
                for (int e = 0; e <= 10; e++) {           // letters
                    for (int d = 0; d <= 10; d++) {
                        for (int g = 0; g <= 10; g++) {

                            char c1 = alphabet.charAt(e);
                            char c2 = alphabet.charAt(d);
                            char c3 = alphabet.charAt(g);

                            nomerok = c1 +
                                    "" + b + "" + b + "" + b +
                                    "" + c2 + "" + c3 + "" + a;

                            list.add(nomerok);
                        }
                    }
                }
            }
        }

        String item = "C000TT16";

        long arl = System.nanoTime();
        list.contains(item);
        long arlt = System.nanoTime() - arl;

        Collections.sort(list);
        long sarl = System.nanoTime();
        int index = Collections.binarySearch(list, item);
        long sarlt = System.nanoTime() - sarl;

        HashSet<String> set = new HashSet<>(list);
        long lhs = System.nanoTime();
        set.contains(item);
        long lhst = System.nanoTime() - lhs;

        TreeSet<String> tree = new TreeSet<>(list);
        long ths = System.nanoTime();
        tree.contains(item);
        long ltst = System.nanoTime() - ths;

        TreeMap<Long, String> results = new TreeMap<>();
        results.put(arlt, "List");
        results.put(sarlt, "Sort List");
        results.put(lhst, "HashSet");
        results.put(ltst, "TreeSet");
        for (Map.Entry<Long, String> entry : results.entrySet()) {
            System.out.println(entry.getValue() + " " + entry.getKey());
        }
    }
}
