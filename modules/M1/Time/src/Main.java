import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        DateFormat format = new SimpleDateFormat("h:mm:s  dd/MM/y");
        Date date = new Date();
        System.out.println("Current time (RU):");
        System.out.println (format.format(date));
        System.out.println("Have a good russian day!");


    }
}
