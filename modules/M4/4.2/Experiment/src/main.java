public class main {
    public static void main(String[] args) {
        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);
        System.out.println(Short.MAX_VALUE);
        System.out.println(Short.MIN_VALUE);
        System.out.println(Long.MAX_VALUE);
        System.out.println(Long.MIN_VALUE);
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        System.out.println("Double max: " + Double.MAX_VALUE);
        System.out.println("Double min: " + -Double.MAX_VALUE);
        System.out.println("Float max: " + Float.MAX_VALUE);
        System.out.println("Float min: " + -Float.MAX_VALUE);
    }
}
