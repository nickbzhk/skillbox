import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            String number = scanner.nextLine();
            System.out.println(correctNumber(number));
        }
    }

    public static String correctNumber(String number) {
        String rightNumber = " ";
        String cleanNumber = number.replaceAll("[^0-9]", "");
        String regex;
        boolean isShort;

        if (cleanNumber.charAt(0) == '9') {
            regex = "(\\d{3})(\\d{3})(\\d{2})(\\d{2})";
            isShort = true;
        } else {
            regex = "(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d{2})";
            isShort = false;
        }

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(cleanNumber);


        if (isShort && matcher.find()) {
            rightNumber = "+7" + " " + matcher.group(1) + " " + matcher.group(2) + "-" + matcher.group(3) + "-" + matcher.group(4);

        } else if (!isShort && matcher.find()) {
            rightNumber = "+7" + " " + matcher.group(2) + " " + matcher.group(3) + "-" + matcher.group(4) + "-" + matcher.group(5);

        } else {
            System.out.println("Error, check spelling.");
        }
        return rightNumber;
    }

    public static boolean isMobilePhoneNumber (String number) {
        if (    (number.length() == 10 && (number.charAt(0) == '9')) ||
                (number.length() > 10 && (number.charAt(0) == '8')) ||
                (number.length() > 10 && (number.charAt(0) == '7')) ||
                (number.length() > 10 && (number.charAt(0) == '+'))) {
            return true;
        } else {
            return false;
        }
    }
}
