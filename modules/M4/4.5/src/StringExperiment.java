import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringExperiment {
    public static void main(String[] args) {
        String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";
        System.out.println(text);

        String numbLine = text.replaceAll("[^0-9,\\,]", "");
        final String regex = "([0-9]+)";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(text);
        int salary;
        int sum = 0;

        while (matcher.find()) {
            //System.out.println(matcher.group());
            salary = Integer.parseInt(matcher.group());
            sum = sum + salary;
        }
        System.out.println("У друзей: " + sum);
    }
}