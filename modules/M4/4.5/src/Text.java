import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    public static void main(String[] args) {
        String s = "Voyager 2 was the first to be launched. Its trajectory was designed to allow flybys of Jupiter, Saturn, Uranus, and Neptune. Voyager 1 was launched after Voyager 2, but along a shorter and faster trajectory that was designed to provide an optimal flyby of Saturn's moon Titan, which was known to be quite large and to possess a dense atmosphere. This encounter sent Voyager 1 out of the plane of the ecliptic, ending its planetary science mission. Had Voyager 1 been unable to perform the Titan flyby, the trajectory of Voyager 2 could have been altered to explore Titan, forgoing any visit to Uranus and Neptune. Voyager 1 was not launched on a trajectory that would have allowed it to continue to Uranus and Neptune, but could have continued from Saturn to Pluto without exploring Titan.";

        final String regex = "([a-zA-Z]+)";
        final String string = "Вася заработал 5000 рублей";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(s);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}
