import java.sql.SQLOutput;

public class Main
{
    public static void main(String[] args)
    {
        Container container = new Container();
        container.count += 7843;

        int sum = sumDigits(12345); // Вызываем метод sumDigits и результат вычислений запоминаем в пепременную sum :)
        System.out.println(sum); // Выводим результат :)
    }

    public static Integer sumDigits (Integer number)
    {
        String stringNumber = String.valueOf(number); // Превращаем число в строку

        char[] charArray = stringNumber.toCharArray(); // Достаем массив символов, для строки "12345" эквивалентно: new char[] {'1', '2', '3', '4', '5'};

        int sum = 0; // Инициализируем переменную для суммы

        for (int i = 0; i < charArray.length; i++) { // делаем цикл по массиву символов
            int intValue = Character.getNumericValue(charArray[i]);// преращаем символ в цифру: '1' -> 1, '5' -> 5 :)
            sum = sum + intValue;  // прибавляем ткущую цифру к текущей сумме
        }

        return sum; // возвращаем сумму из метода
    }
}