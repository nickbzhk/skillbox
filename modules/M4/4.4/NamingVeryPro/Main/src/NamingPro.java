import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NamingPro {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ваше имя, фамилию и отчество:");
        String input = scanner.nextLine();

        if (containsDigits(input) || spaceCount(input) != 2) {
            System.out.println("Ошибка, проверьте правильность ввода");
        } else {
            final String regex = "([а-яА-Яa-zA-Z]+) ([а-яА-Яa-zA-Z]+) ([а-яА-Яa-zA-Z]+)";

            final Pattern pattern = Pattern.compile(regex);
            final Matcher matcher = pattern.matcher(input);

            if (matcher.find()) {
                System.out.println("Фамилия: " + matcher.group(1));
                System.out.println("Имя: " + matcher.group(2));
                System.out.println("Отчество: " + matcher.group(3));
            }
        }
    }

    public static boolean containsDigits (String input) {
        char[] inputArray = input.toCharArray();

        int digitCount = 0;
        for (char c : inputArray) {
            if (Character.isDigit(c)) {
                digitCount++;
            }
        }

        boolean digit = false;
        digit = digitCount >= 1;

        return digit;
    }

    public static int spaceCount (String input) {
        char[] inputArray = input.toCharArray();

        int spaceCount = 0;
        for (char c : inputArray) {
            if (Character.isWhitespace(c)) {
                spaceCount++;
            }
        }
        return spaceCount;
    }
}
