package Naming;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ваше имя, фамилию и отчество:");
        String input = scanner.nextLine();

        String input2 = input + "$";
        int spaceIndex = input.indexOf(' ');
        int spaceIndex2 = input.lastIndexOf(' ');
        int lastIndex = input2.lastIndexOf('$');

        System.out.println("Фамилия: " + input.substring(0, spaceIndex));
        System.out.println("Имя: " + input.substring(spaceIndex, spaceIndex2).trim());
        System.out.println("Отчество: " + input.substring(spaceIndex2, lastIndex).trim());
    }
}
