import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Scanner;
import java.util.spi.CalendarDataProvider;

public class CalendarAges {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите год вашего рождения:");
        short year = sc.nextShort();                                                   // Year
        System.out.println("Введите число месяца вашего рождения:");
        byte month = sc.nextByte();                                                    // Month
        System.out.println("Введите число дня месяца вашего рождения:");
        byte day = sc.nextByte();                                                      // Day

        LocalDate ld = LocalDate.of(year, month, day);                                 // Birth day
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("d.MM.YYYY - EEE");

        short currentYear = (short) LocalDate.now().getYear();                         // Current year
        final short CURRENT_AGE = (short) (currentYear - year);                        // Age finder
        byte ageCount = 0;

        for (byte countUp = 0; countUp <= CURRENT_AGE + 1; countUp++) {
            ageCount = countUp;
            LocalDate ldReturn = ld.plusYears(countUp);                                // Year setter
            String birthDate = ldReturn.format(timeFormat);

            System.out.println    (ageCount + "-й год - " + birthDate);                // Out
        }
    }
}