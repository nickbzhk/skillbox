import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Ввод
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите кол-во контейнеров: ");
        int input = sc.nextInt();

        //Константы (Дано)
        final int TRUCK_CAPACITY = 12; //контейнеров
        final int CONTAINER_CAPACITY = 27; // ящиков

        int boxId = 1; // нумерация ящиков

        int containerCount = input / CONTAINER_CAPACITY;
        int truckCount = containerCount / TRUCK_CAPACITY;

        if (input % CONTAINER_CAPACITY != 0) {
            containerCount++;
        }
        if (containerCount % TRUCK_CAPACITY != 0) {
            truckCount++;
        }

        for (int i = 1; i <= truckCount; i++) {
            System.out.println("\n" + "Грузовик #" + i);
            if (boxId > input) {
                break;
            }
            for (int a = 1; a <= TRUCK_CAPACITY; a++) {
                if (boxId > input) {
                    break;
                }
                System.out.println("\n" + "\t" + "Контейнер #" + a + "\n");
                for (int b = 1; b <= CONTAINER_CAPACITY; b++) {
                    System.out.println("\t" + "\t" + "Ящик #" + boxId++);
                    if (boxId > input) {
                        break;
                    }
                }
            }
        }
    }
}