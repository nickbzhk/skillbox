import com.companyTools.EmployeeTool.*;

public class Loader {
    public static Company company = new Company();

    public static void main(String[] args) {

        generateEmployee("operator", 180);
        generateEmployee("manager", 80);
        generateEmployee("top manager", 10);
        company.generateMonthlySelling();

        printStatistics();
        fireUnproductiveLoosersNow();
    }

    public static void printStatistics() {
        System.out.println("\n Top 5");
        getTopSalaryStaff(5);
        System.out.println("\n Top 5 sellers");
        getTopSellingStaff(5);

        System.out.println("\n Lowest 5");
        getLowestSalaryStaff(5);
        System.out.println("\n Lowest 5 sellers");
        getLowestSellingStaff(5);

        System.out.println("============================================================================");

        System.out.println("\n TOTAL INCOME: \n" + company.getIncome() + "\n");
        System.out.println("\n TOTAL EMPLOYEES: \n" + company.getCompanySize() + "\n");
        System.out.println("\n Loosers (" + company.getLoosersAmount() + ")");


    }

    public static void fireUnproductiveLoosersNow() {
        System.out.println(company.getLoosersAmount() + " bad employees were fired");
        company.fireUnproductiveLoosersNow();
    }

    public static void generateEmployee(String employee, int amount) {
        for (int i = 0; i <= amount - 1; i++) {
            Integer count = i;
            if (employee.equals("operator")) {
                String name = "Operator";
                company.hire(new Operator(name));
            }
            if (employee.equals("manager")) {
                String name = "Manager";
                company.hire(new Manager(name));
            }
            if (employee.equals("top manager")) {
                String name = "Top Manager";
                company.hire(new TopManager(name));
            }
        }
    }

    public static void getLoosersList() {
        System.out.println(company.getLoosersAmount());
        if (!(company.getLoosersAmount() == 0)) {
            for (int y = 0; y <= company.getLoosersAmount() - 1; y++) {
                System.out.println("Employee: " + company.getLoosersList().get(y).getName());
                System.out.print("Sellings: " + company.getLoosersList().get(y).getSellings());
                System.out.println(" Salary: " + company.getLoosersList().get(y).getMonthSalary() + "\n");
            }
        } else {
            System.out.println("NO LOOSERS!");
        }
    }

    public static void getTopSalaryStaff(int count) {
        for (int i = 0; i <= count - 1; i++) {
            System.out.println("Employee: " + company.getTopSalaryStaff().get(i).getName());
            System.out.print("Sellings: " + company.getTopSalaryStaff().get(i).getSellings());
            System.out.println(" Salary: " + company.getTopSalaryStaff().get(i).getMonthSalary() + "\n");
        }
    }

    public static void getLowestSalaryStaff(int count) {
        for (int i = company.getLowestSalaryStaff().size() - 1; i >= company.getLowestSalaryStaff().size() - count; i--) {
            System.out.println("Employee: " + company.getLowestSalaryStaff().get(i).getName());
            System.out.print("Sellings: " + company.getLowestSalaryStaff().get(i).getSellings());
            System.out.println(" Salary: " + company.getLowestSalaryStaff().get(i).getMonthSalary() + "\n");
        }
    }

    public static void getTopSellingStaff(int count) {
        for (int i = 0; i <= count - 1; i++) {
            System.out.println("Employee: " + company.getTopSellingStaff().get(i).getName());
            System.out.print("Sellings: " + company.getTopSellingStaff().get(i).getSellings());
            System.out.println(" Salary: " + company.getTopSellingStaff().get(i).getMonthSalary() + "\n");
        }
    }

    public static void getLowestSellingStaff(int count) {
        for (int i = company.getLowestSalaryStaff().size() - 1; i >= company.getLowestSalaryStaff().size() - count; i--) {
            System.out.println("Employee: " + company.getLowestSellingStaff().get(i).getName());
            System.out.print("Sellings: " + company.getLowestSellingStaff().get(i).getSellings());
            System.out.println(" Salary: " + company.getLowestSellingStaff().get(i).getMonthSalary() + "\n");
        }
    }
}
