package com.companyTools.EmployeeTool;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Manager implements Employee {
    final double MANAGER_SALARY_FIXED = 70000.00;
    final double ONE_UNIT_PRICE = 100000.;
    final BigDecimal GOAL = BigDecimal.valueOf(10000);
    private BigDecimal salary = BigDecimal.valueOf(0.);
    private BigDecimal selling = BigDecimal.valueOf(0.);
    private String name;

    public Manager(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getSellings() {
        return selling.setScale(2, RoundingMode.CEILING);
    }

    @Override
    public BigDecimal generateSelling() {
        this.selling = selling.add(BigDecimal.valueOf(ONE_UNIT_PRICE * Math.random()));
        return selling;
    }

    public boolean isLooser() {
        return (selling.compareTo(GOAL) == -1);
    }

    @Override
    public BigDecimal getMonthSalary() {
        return salary.setScale(2, RoundingMode.CEILING);
    }

    @Override
    public void addMonthSalary(BigDecimal income) {
        this.salary = salary.add(BigDecimal.valueOf(MANAGER_SALARY_FIXED).add(getSellings().multiply(BigDecimal.valueOf(0.05))));
    }
}
