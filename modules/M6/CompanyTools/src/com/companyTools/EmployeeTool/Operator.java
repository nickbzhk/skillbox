package com.companyTools.EmployeeTool;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Operator implements Employee {
    final double OPERATOR_SALARY_FIXED = 40000.00;
    final double ONE_UNIT_PRICE = 63000.;
    final BigDecimal GOAL = BigDecimal.valueOf(12000.);
    private BigDecimal salary = BigDecimal.valueOf(0.);
    private BigDecimal selling = BigDecimal.valueOf(0.);
    private String name;

    public Operator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public BigDecimal generateSelling() {
        selling = selling.add(BigDecimal.valueOf(ONE_UNIT_PRICE * Math.random()));
        return selling;
    }

    @Override
    public BigDecimal getSellings() {
        return selling.setScale(2, RoundingMode.CEILING);
    }

    public boolean isLooser() {
        return (selling.compareTo(GOAL) == -1);
    }

    @Override
    public BigDecimal getMonthSalary() {
        return salary.setScale(2, RoundingMode.CEILING);
    }

    @Override
    public void addMonthSalary(BigDecimal income) {
        this.salary = salary.add(BigDecimal.valueOf(OPERATOR_SALARY_FIXED));
    }
}
