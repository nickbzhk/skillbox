package com.companyTools.EmployeeTool;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TopManager implements Employee {
    double TOP_MANAGER_SALARY_FIXED = 80000.00;
    final double ONE_UNIT_PRICE = 200000.;
    final BigDecimal GOAL = BigDecimal.valueOf(30000.);
    private BigDecimal salary = BigDecimal.valueOf(0.);
    private BigDecimal selling = BigDecimal.valueOf(0.);
    private String name;

    public TopManager(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public BigDecimal getSellings() {
        return this.selling.setScale(2, RoundingMode.CEILING);
    }

    @Override
    public BigDecimal generateSelling() {
        this.selling = selling.add(BigDecimal.valueOf(ONE_UNIT_PRICE * Math.random()));
        return this.selling;
    }

    public boolean isLooser() {
        return (selling.compareTo(GOAL) == -1);
    }

    @Override
    public BigDecimal getMonthSalary() {
        return this.salary.setScale(2, RoundingMode.CEILING);
    }

    @Override
    public void addMonthSalary(BigDecimal income) {
        if ((income.compareTo(BigDecimal.valueOf(10000000.00)) == 1)) {
            this.salary = salary.add(BigDecimal.valueOf(TOP_MANAGER_SALARY_FIXED)).add(BigDecimal.valueOf(TOP_MANAGER_SALARY_FIXED * 1.5));
        } else {
            this.salary = salary.add(BigDecimal.valueOf(TOP_MANAGER_SALARY_FIXED));
        }
    }
}
