fun main (args: Array<String>) {
    val FIRST_NAME = "Nikolay"
    val SECOND_NAME = "Bozhok"
    var age = 25
    var weight: Float = 70.0f
    var height = 185
    var isAdult: Boolean = true

    if (height <= 150 || weight <= 45) {
        isAdult = false
    }

    val info = """
        User name: $FIRST_NAME $SECOND_NAME
        ${age}y.o., ${weight}kg, ${height}cm
        Identified as: ${if (isAdult) {"Adult"} else {"Children"}}
    """

    print (info)
}