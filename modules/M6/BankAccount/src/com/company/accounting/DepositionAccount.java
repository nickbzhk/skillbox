package com.company.accounting;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class DepositionAccount extends MoneyAccount {

    private LocalDateTime nextOperationDate = LocalDateTime.now();

    @Override
    public void putMoney(BigDecimal amount) {
        super.putMoney(amount);
        nextOperationDate = getCurrentDate().plusMonths(1);
    }

    @Override
    public void takeMoney(BigDecimal amount) {
        if (isAvailable()) {
            super.takeMoney(amount);
        } else {
            System.out.println("You will be able to take money at " + nextOperationDate.format(format));
        }
    }

    private boolean isAvailable() {
        return getCurrentDate().isAfter(nextOperationDate) || getCurrentDate().equals(nextOperationDate);
    }
}
