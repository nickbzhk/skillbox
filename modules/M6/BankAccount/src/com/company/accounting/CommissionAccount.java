package com.company.accounting;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CommissionAccount extends MoneyAccount {
    @Override
    public void takeMoney(BigDecimal amount) {
        BigDecimal commission = amount.multiply(BigDecimal.valueOf(0.01));
        if (super.getBalance().subtract(amount).subtract(commission).compareTo(BigDecimal.valueOf(0)) == -1){
            System.out.println("Not enough money!");
        } else {
            super.takeMoney(amount);
            super.takeMoney(commission);
        }
    }
}
