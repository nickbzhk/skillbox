package com.company.accounting;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoneyAccount {


    private LocalDateTime currentDate = LocalDateTime.now();
    protected DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    public LocalDateTime getCurrentDate() {
        return currentDate = LocalDateTime.now();
    }

    public String getCurrentDateFormatted() {
        currentDate = LocalDateTime.now();
        return currentDate.format(format);
    }

    protected StringBuilder sb = new StringBuilder();
    protected BigDecimal balance = BigDecimal.valueOf(0);

    public void getBalanceMessage() {
        System.out.println("Balance: " + balance.setScale(2, RoundingMode.FLOOR) + "\u20BD");
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void putMoney(BigDecimal amount) {
        balance = balance.add(amount);
        sb.delete(0, sb.length()).append('+').append(amount).append("\u20BD");
        addOperationRecord(sb.toString());
        System.out.println(sb);
        getBalanceMessage();
    }

    public void takeMoney(BigDecimal amount) {
        if (balance.subtract(amount).compareTo(BigDecimal.valueOf(0)) == -1) {
            System.out.println("Not enough money!");
        } else {
            balance = balance.subtract(amount);
            sb.delete(0, sb.length()).append('-').append(amount).append("\u20BD");
            addOperationRecord(sb.toString());
            System.out.println(sb);
            getBalanceMessage();
        }
    }

    private ArrayList<String> operationRecords = new ArrayList<>();

    public void addOperationRecord(String operation) {
        operationRecords.add(getCurrentDateFormatted() + ":   " + operation);
    }

    public void getOperationRecordsList() {
        for (int i = 0; i <= operationRecords.size() - 1; i++)
            System.out.println(operationRecords.get(i));
    }
}
