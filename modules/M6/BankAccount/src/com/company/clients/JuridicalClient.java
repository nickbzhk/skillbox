package com.company.clients;

import java.math.BigDecimal;

public class JuridicalClient extends Client {
    @Override
    protected BigDecimal getPutCommission(BigDecimal amount) {
        return BigDecimal.valueOf(0);
    }

    @Override
    protected BigDecimal getTakeCommission(BigDecimal amount) {
        this.commission = amount.multiply(BigDecimal.valueOf(0.01));
        return commission;
    }
}
