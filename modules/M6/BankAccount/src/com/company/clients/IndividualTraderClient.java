package com.company.clients;

import java.math.BigDecimal;

public class IndividualTraderClient extends Client {
    @Override
    protected BigDecimal getTakeCommission(BigDecimal amount) {
        return BigDecimal.valueOf(0);
    }

    @Override
    protected BigDecimal getPutCommission(BigDecimal amount) {
        this.commission = (amount.compareTo(BigDecimal.valueOf(1000)) == -1) ? amount.multiply(BigDecimal.valueOf(0.01)) : amount.multiply(BigDecimal.valueOf(0.005));
        return commission;
    }
}
