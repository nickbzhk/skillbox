package com.company.clients;

import java.math.BigDecimal;

public class PhysicalClient extends Client {

    @Override
    protected BigDecimal getTakeCommission(BigDecimal amount) {
        return BigDecimal.valueOf(0);
    }

    @Override
    protected BigDecimal getPutCommission(BigDecimal amount) {
        return BigDecimal.valueOf(0);
    }
}
