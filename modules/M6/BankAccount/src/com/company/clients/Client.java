package com.company.clients;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public abstract class Client {

    private BigDecimal balance = BigDecimal.valueOf(0);

    protected StringBuilder sb = new StringBuilder();

    public BigDecimal getBalance() {
        return balance;
    }

    public void getBalanceMessage() {
        System.out.println("Balance: " + balance.setScale(2, RoundingMode.FLOOR) + "\u20BD");
    }

    protected BigDecimal commission;

    protected abstract BigDecimal getTakeCommission(BigDecimal amount);
    protected abstract BigDecimal getPutCommission(BigDecimal amount);

    public void putMoney(BigDecimal amount) {
        balance = balance.add(amount).subtract(getPutCommission(amount));
        sb.delete(0, sb.length()).append('+').append(amount).append("\u20BD").append(" (commission ").append(getPutCommission(amount)).append("\u20BD)");
        addOperationRecord(sb.toString());
        System.out.println(sb);
        getBalanceMessage();
    }

    public void takeMoney(BigDecimal amount) {
        if (balance.subtract(amount).compareTo(BigDecimal.valueOf(0)) == -1) {
            System.out.println("Not enough money!");
        } else {
            balance = balance.subtract(amount.add(getTakeCommission(amount)));
            sb.delete(0, sb.length()).append('-').append(amount).append("\u20BD").append(" (commission ").append(getTakeCommission(amount)).append("\u20BD)");
            addOperationRecord(sb.toString());
            System.out.println(sb);
            getBalanceMessage();
        }
    }

    private final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    private LocalDateTime currentDate = LocalDateTime.now();

    private String getCurrentDateTimeFormat() {
        currentDate = LocalDateTime.now();
        return currentDate.format(DATE_FORMAT);
    }

    private final ArrayList<String> OPERATIONS = new ArrayList<>();

    public void addOperationRecord(String operation) {
        OPERATIONS.add(getCurrentDateTimeFormat() + ":   " + operation);
    }

    public void getOperationRecordsList() {
        for (int i = 0; i <= OPERATIONS.size() - 1; i++)
            System.out.println(OPERATIONS.get(i));
    }
}
