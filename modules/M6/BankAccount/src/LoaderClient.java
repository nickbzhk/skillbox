import com.company.clients.Client;
import com.company.clients.IndividualTraderClient;
import com.company.clients.JuridicalClient;
import com.company.clients.PhysicalClient;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoaderClient {
    private static void welcomeMessage() {
        System.out.println("Welcome to Money Account (Client edition)! " +
                "Type these commands:\n" +
                "\"PUT\"  -> Put money (amount),\n" +
                "\"TAKE\" -> Take money (amount),\n" +
                "\"SHOW\" -> Show balance,\n" +
                "\"LIST\" -> Operations Journal."
        );
    }

    public static void main(String[] args) {
        welcomeMessage();
        Client client = new PhysicalClient();
        for (; ; ) {
            Scanner s = new Scanner(System.in);
            System.out.print(">:");
            String input = s.nextLine() + '.';
            action(client, input);
        }
    }

    private static void action(Client client, String input) {
        Pattern pattern = Pattern.compile("([A-Z]*)\\s*((\\d+\\.{1}\\d*)|(\\d*))");
        Matcher matcher = pattern.matcher(input);
        BigDecimal amount;

        if (matcher.find()) {
            String getCommand = matcher.group(1);
            if (input.contains("PUT") || input.contains("TAKE")) {
                amount = BigDecimal.valueOf(Double.parseDouble(matcher.group(2)));
            } else {
                amount = BigDecimal.valueOf(0);
            }
            switch (getCommand) {
                case "PUT" -> client.putMoney(amount);
                case "TAKE" -> client.takeMoney(amount);
                case "SHOW" -> client.getBalanceMessage();
                case "LIST" -> client.getOperationRecordsList();
            }
        }
    }
}
