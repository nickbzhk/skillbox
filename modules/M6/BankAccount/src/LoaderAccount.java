import com.company.accounting.CommissionAccount;
import com.company.accounting.DepositionAccount;
import com.company.accounting.MoneyAccount;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoaderAccount {
    private static void welcomeMessage() {
        System.out.println("Welcome to Money Account! " +
                "Type these commands:\n" +
                "\"PUT\"  -> Put money (amount),\n" +
                "\"TAKE\" -> Take money (amount),\n" +
                "\"SHOW\" -> Show balance,\n" +
                "\"LIST\" -> Operations Journal."
        );
    }

    public static void main(String[] args) {
        welcomeMessage();
        MoneyAccount accountType = new MoneyAccount();
        for (; ; ) {
            Scanner s = new Scanner(System.in);
            System.out.print(">:");
            String input = s.nextLine() + '.';
            action(accountType, input);
        }
    }

    private static void action(MoneyAccount accountType, String input) {
        Pattern pattern = Pattern.compile("([A-Z]*)\\s*((\\d+\\.{1}\\d*)|(\\d*))");
        Matcher matcher = pattern.matcher(input);
        BigDecimal amount;

        if (matcher.find()) {
            String getCommand = matcher.group(1);
            if (input.contains("PUT") || input.contains("TAKE")) {
                amount = BigDecimal.valueOf(Double.parseDouble(matcher.group(2)));
            } else {
                amount = BigDecimal.valueOf(0);
            }
            switch (getCommand) {
                case "PUT" -> accountType.putMoney(amount);
                case "TAKE" -> accountType.takeMoney(amount);
                case "SHOW" -> accountType.getBalanceMessage();
                case "LIST" -> accountType.getOperationRecordsList();
            }
        }
    }
}
