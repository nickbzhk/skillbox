import java.util.Scanner;

public class Main
{   //Доход компании
    private static int minIncome = 200000;
    private static int maxIncome = 900000;
    //Затраты
    private static int officeRentCharge = 140000;
    private static int telephonyCharge = 12000;
    private static int internetAccessCharge = 7200;
    //ЗП сотрудников
    private static int assistantSalary = 45000;
    private static int financeManagerSalary = 90000;
    //Налоги
    private static double mainTaxPercent = 0.24;
    private static double managerPercent = 0.15;
    //Минимальная сумма чистой прибыли после вычета для инвестиции
    private static double minInvestmentsAmount = 100000;

    public static void main(String[] args)
    {   //Параметр цикличности ввода
        while(true)
        {
            System.out.println("Введите сумму доходов компании за месяц " +
                "(от 200 до 900 тысяч рублей): ");
            //Здесь вводится доход
            int income = (new Scanner(System.in)).nextInt();

            //Этот параметр проверяет условия диапозона сумм которые можно вводить выше
            if(!checkIncomeRange(income)) {
                continue; //Переход к след. методу если сумма больше или меньше охвата диапозоном
            }
            //Умножение
            double managerSalary = income * managerPercent; //ЗП менеджера с вычетом
            double pureIncome = income - managerSalary -
                calculateFixedCharges(); //Чистая прибыль
            double taxAmount = mainTaxPercent * pureIncome;//Прибыль  416 666
            double pureIncomeAfterTax = pureIncome - taxAmount;//с вычетом

            boolean canMakeInvestments = pureIncomeAfterTax >=
                minInvestmentsAmount; //Проверка возможности делать инвестиции

            System.out.println("Зарплата менеджера: " + managerSalary);
            System.out.println("Общая сумма налогов: " +
                (taxAmount > 0 ? taxAmount : 0));
            System.out.println("Компания может инвестировать: " +
                (canMakeInvestments ? "да" : "нет"));
            if(pureIncome < 0) {
                System.out.println("Бюджет в минусе! Нужно срочно зарабатывать!");
            }
        }
    }

    private static boolean checkIncomeRange(int income) //Метод проверки введённой выше суммы
    {
        if(income < minIncome)
        {
            System.out.println("Доход меньше нижней границы");
            return false;
        }
        if(income > maxIncome)
        {
            System.out.println("Доход выше верхней границы");
            return false;
        }
        return true;
    }

    private static int calculateFixedCharges() //Метод расчёта выделяемого бюджета
    {
        return officeRentCharge +
                telephonyCharge +
                internetAccessCharge +
                assistantSalary +
                financeManagerSalary;
    }
}
